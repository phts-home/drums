;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; �������� ��� ������ ���������� ������� "����� ������� ���������"
;;;
;;; ��� ������ CLIPS - 6.3
;;;
;;;
;;; ������:  0.1.1.400
;;; �����:   ������ �����
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;






;;;;;;;;;;;;;;;;;
;;; DEFGLOBAL
;;;

(defglobal ?*min-money* = 20)






;;;;;;;;;;;;;;;;;
;;; FACTS
;;;

; name             - ��� ������������
; money            - ����� ����� � ���������� (��� -1)
; money-enough     - ���������� �� ����� (������ ��� ?*min-money*) (yes, no)
; type             - ��� ����������� (acoustic, electronic, practice)
; state            - ������� ��������� ������� (start, stop, restart)

; skill            - ������� ����������� (beginner, student, pro, unknown)
; exp              - ���� ����������� (less-1, from-1-to-3, more-3)
; education        - ����������� (none, medium, high)

; ac-usage         - ������������� ������������ ��������� (training, repetitions, concerts, recording)
; ac-jenre         - ���� ������ (rock, metal, jazz, fusion, electronic, unknown)
; ac-to-el         - "�������� �� ������������ ������� ����������� ������ ������������" (yes, no)
; ac-drumkit-level - ������� ������������ ������� ��������� (0, 1, 2, 3, 4)
; ac-band          - �������� ������, �������� � ������� �����
; ac-found-model-ids - id ��������� ������� ������������ ��������� (�������� ���� MULTIFIELD, ��� ������ NOTHING-FOUND ���� ������ �� �������)
; ac-found-model-better-id - id ����� �������, �� ����� ��������� (���� ������ money), ������ ������������ ��������� (�������� ���� INTEGER, ��� NOTHING-FOUND ���� ������ �� �������)
; ac-found-model-money-best-id - id ������ ������������ ���������, ��������� ������� ������������ � ������ money (�������� ���� INTEGER, ��� NOTHING-FOUND ���� ������ �� �������)

; el-type          - ��� ����������� ������� ��������� (drumkit, percussion, unknown)
; el-exist         - ����� �� ������ "���� �� ��� ���������" (yes, no)
; el-agree         - "�������� �� ������������ ������ ��������� � ���������� � ������������ ���������" (yes, no)
; el-level         - ������� ����������� ������� ��������� (0, 1, 2)
; el-found-model-ids   - id ��������� ������� ����������� ��������� (�������� ���� MULTIFIELD, ��� ������ NOTHING-FOUND ���� ������ �� �������)
; el-found-model-better-id - id ����� �������, �� ����� ��������� (���� ������ money), ������ ����������� ��������� (�������� ���� INTEGER, ��� NOTHING-FOUND ���� ������ �� �������)
; el-found-model-money-best-id - id ������ ����������� ���������, ��������� ������� ������������ � ������ money (�������� ���� INTEGER, ��� NOTHING-FOUND ���� ������ �� �������)

; pr-type          - ��� ������������� ��������� (kit, pad, unknown)
; pr-size          - ������ ���� (small, normal)
; pr-rudiments     - ����� �� ������, ����� �� ������������ ��������� (yes, no, dontknow)
; pr-found-kit-model-id - id ��������� ������ ������������� ��������� (�������� ���� INTEGER, ��� ������ NOTHING-FOUND ���� ������ �� �������)
; pr-found-kit-model-better-id - id ����� �������, �� ����� ��������� (���� ������ money), ������ ������������� ��������� (�������� ���� INTEGER, ��� NOTHING-FOUND ���� ������ �� �������)
; pr-found-pad-model-id - id ��������� ������ �������������� ���� (�������� ���� INTEGER, ��� ������ NOTHING-FOUND ���� ������ �� �������)
; pr-found-pad-model-better-id - id ����� �������, �� ����� ��������� (���� ������ money), ������ �������������� ���� (�������� ���� INTEGER, ��� NOTHING-FOUND ���� ������ �� �������)






;;;;;;;;;;;;;;;;;
;;; DEFFUNCTION
;;;

; ���������� �������� �� ����������� ����� ������
; ?sec - ���������� ������, ��� NUMBER
(deffunction sleep (?sec)
  (if (< ?sec 0) then
    (return TRUE)
  )
  
  (bind ?t1 (time))
  (bind ?t2 (time))
  (while (< (- ?t2 ?t1) ?sec) do
    (bind ?t2 (time))
  )
  
  (return TRUE)
)

; ������� �� ����� ����� � ����������� ���������
; $?text - ����� ��� ������ �� �����
(deffunction print-animated-text ($?text)
  (bind ?word-count (length $?text))
  (bind ?len 1)
  (while (<= ?len ?word-count) do
    
    (bind ?word (nth ?len $?text))
    
    (if (or (numberp ?word) (eq ?word crlf)) then
      (printout t ?word)
    else
      (bind ?word-len (str-length ?word))
      (bind ?i 1)
      (while (<= ?i ?word-len) do
        (printout t (sub-string ?i ?i ?word))
        (bind ?i (+ ?i 1))
        (sleep 0.015)
      )
    )
    
    (bind ?len (+ ?len 1))
  )
)

; ������� �� ����� ��������������� ��������� ���������
; $?message - ����� ���������
(deffunction print-simple-message ($?message)
  (printout t "  -- " )
  (bind ?len 1)
  (while (<= ?len (length $?message)) do
    (printout t (nth ?len $?message) ) 
    (bind ?len (+ ?len 1))
  )
  
  (sleep 1)
  
  (printout t crlf)
)

; ������� �� ����� ��������� ��������� ��� �������� ����� ����
; $?message - ����� ���������
(deffunction print-message-without-delay ($?message)
  (printout t "  -- " )
  (print-animated-text ?message)
  
  (printout t crlf)
)

; ������� �� ����� ��������� ��������� � ���� 2 ������� ����� ����
; $?message - ����� ���������
(deffunction print-message ($?message)
  (printout t "  -- " )
  (print-animated-text ?message)
  
  (sleep 1.5)
  
  (printout t crlf)
)



; ������� �� ����� ��������� � ������ ������
(deffunction message-hello ()
  (print-animated-text "")
  (printout t crlf crlf crlf)
  (print-message "����������...")
  (print-message-without-delay "����������� ���, ������������. ���� ����� \"����� ������� ���������\"")
  (print-message "� ���� ������� ��� ����, ����� ������ ��� � ������ ������� ���������")
)

; ������� �� ����� ��������� � ���������� ������
(deffunction message-goodbye (?name)
  (printout t crlf crlf)
  (print-message "�� ��������, " ?name ". ����������� ������ ����� ��������� :-)")
  (printout t crlf crlf)
  (print-animated-text "(�) ������ �����, 2010")
  (sleep 1)
  (printout t crlf)
)

; ������� �� ����� ������ ?question � ���� ������ �� ������������
; ?question - ������
; ?variants - ������ � �������������� ��������� ������, �������� "(1-��; 2-���; 3-�� ����)"
; $?allowed-values - �������� �������. ���� �� �������, �� ������������ ��������� ��������
; ���������� �����
(deffunction ask-question (?question ?variants $?allowed-values)
  (printout t "> ")
  (print-animated-text ?question " ")
  (if (neq (length $?allowed-values) 0) then
    (sleep 0.3)
    (printout t crlf "        " ?variants " ")
  )
  (bind ?answer (read))
  (if (lexemep ?answer) then
    (bind ?answer (lowcase ?answer))
  )
  (if (neq (length $?allowed-values) 0) then
    (while (not (member ?answer ?allowed-values)) do
      (print-message "�� ������...")
      (printout t "> ")
      (print-animated-text ?question " ")
      (if (neq (length $?allowed-values) 0) then
        (sleep 0.3)
        (printout t crlf "        " ?variants " ")
      )
      (bind ?answer (read))
      (if (lexemep ?answer) then
        (bind ?answer (lowcase ?answer))
      )
    )
  )
  
  ?answer
)

; ������ ������ ���� "?question (1-?yes-text; 2-?no-text)"
; ?question - ���������� ������
; ?yes-text - �������� �������������� �������� ������
; ?no-text - �������� �������������� �������� ������
; ?yes-fact-val - �������� �����, ������������ ��� ������������� ������
; ?no-fact-val - �������� �����, ������������ ��� ������������� ������
; ?yes-message - ��������� ������� ��� ������ "��"
; ?no-message - ��������� ������� ��� ������ "���"
; ���������� ?yes-fact-val ��� ?no-fact-val, � ����������� �� ������
(deffunction ask-2-param-question (?question ?yes-text ?no-text ?yes-fact-val ?no-fact-val ?yes-message ?no-message)
  (bind ?response (ask-question ?question (str-cat "(1-" ?yes-text "; 2-" ?no-text ") ") 1 2))
  (if (eq ?response 1) then
    (bind ?fact-val ?yes-fact-val)
    (if (neq ?yes-message "") then
      (print-message ?yes-message)
    )
  )
  (if (eq ?response 2) then
    (bind ?fact-val ?no-fact-val)
    (if (neq ?no-message "") then
      (print-message ?no-message)
    )
  )
  ?fact-val
)

; ������ ������, ������� ����� ����� �� ��� ���
; ?question - ���������� ������
; ?yes-message - ��������� ������� ��� ������ "��"
; ?no-message - ��������� ������� ��� ������ "���"
; ���������� yes ��� no, � ����������� �� ������
(deffunction ask-yes-or-no (?question ?yes-message ?no-message)
  (bind ?fact-val (ask-2-param-question ?question "��" "���" yes no ?yes-message ?no-message))
  ?fact-val
)



; ���������� ��� ������������
(deffunction ask-name () 
  (printout t "> ")
  (print-animated-text "��� ������, ����������, �������������, ����� � ����� � ��� � ���� ��������: ")
  (bind ?response (read))
  (print-message "����� �������, " ?response)
  ?response 
)

; ���������� ������������, ����� ������ �������� �� �����������
; ���������� �������� ����� �����, ��� NUMBER
(deffunction ask-money ()
  (bind ?response (ask-question "����� ����� � $$ �� ������ ��������� �� ������� ������� ���������? (��������������� �����) " ""))
  
  (while (or (not (numberp ?response)) (< ?response 0)) do
  
    (if (not (numberp ?response)) then
      (print-message "����� �������� ������ ������������ �����, � ������ �������, �����")
      (bind ?response (ask-question "����������, ������� ����� ������: " ""))
      
      (if (not (numberp ?response)) then
        (print-message "�� ������ ������, ��� �������� �����? ��� ������� ������ �� ���� (1,2,3,4,5,6,7,8,9,0)")
        (bind ?response (ask-question "����������, ������� ����� ������ ��� ����� (��������, 123):" ""))
        
        (if (not (numberp ?response)) then
          (print-message "�-�-�-�... ���... ������... ")
          (ask-question "��������� �� �������, ��... �� �����?" "")
          (print-message "�� ����� ������ ����� � ���� ����")
          (return -1)
        )
      )
    )
    
    (if (< ?response 0) then 
      (print-message "����� �������� ������ ������������ ����� ��������������� �����")
      (bind ?response (ask-question "����������, ������� ����� ������: " ""))
    )
    
    (if (and (numberp ?response) (>= ?response 0)) then 
      (print-message "�������, ��� ���� ������ � ��������� ������������� � ����� � ���� ���������")
      (break)
    )
  
  )
  
  ?response
)

; ���������, ���������� �� �����
; ?m - ������ ������������, ��� NUMBER
; ���������� �������� �����, ����������� �� ��� ������
(deffunction check-money ( ?m )
  (if (< ?m ?*min-money*) then
    (bind ?fact-val no)
    (print-message "�� ����� ������ �� ������ �� ������")
  else
    (bind ?fact-val yes)
  )
  ?fact-val
)


; ���������� ������������, ����� �� �� ������ ������ ����� ��������
; ���������� �������� �����, ����������� �� ��� ������
(deffunction ask-restart ()
  (bind ?fact-val (ask-2-param-question "������ ������ ������?" "��" "���" restart stop "" ""))
  ?fact-val
)

; ���������� ������������, ����� ��� ��������� �� �����
; ���������� �������� �����, ����������� �� ��� ������
(deffunction ask-type ()
  (bind ?response (ask-question "����� ���� �� �����������, ������� ������� ���������?" "(1-���� ������,����������,���������; 2-��� ���� �/��� ������ � ���.��������; 3-������ ��� ����������) " 1 2 3))
  (if (eq ?response 1) then
    (bind ?fact-val acoustic)
    (print-message "� �������, ��� ����� ������������ ������� ��������� - �� ��� ����� ������ ��")
  )
  (if (eq ?response 2) then
    (bind ?fact-val electronic)
    (print-message "�����, ����������� ������� ��������� ���� �� ��� ��� �������� ��������")
  )
  (if (eq ?response 3) then
    (bind ?fact-val practice)
    (print-message "�����, ����� ��� ����� ������������� ���� - ������ � �������� �����")
  )
  ?fact-val
)

; ���������� ������������, ��� ���� ����� �������������� ���������
; ���������� �������� �����, ����������� �� ��� ������
(deffunction ask-ac-usage ()
  (bind ?response (ask-question "��� ���� ��� ������� ���������?" "(1-��� ��������; 2-��� ���������; 3-��� ����� �����������; 4-��� �����������) " 1 2 3 4))
  (if (eq ?response 1) then
    (bind ?fact-val training)
  )
  (if (eq ?response 2) then
    (bind ?fact-val repetitions)
  )
  (if (eq ?response 3) then
    (bind ?fact-val concerts)
  )
  (if (eq ?response 4) then
    (bind ?fact-val recording)
  )
  ?fact-val
)

; ���������� ������������, ����� � ���� �������
; ���������� �������� �����, ����������� �� ��� ������
(deffunction ask-skill ()
  (bind ?response (ask-question "�������, ����������, ���� ������� ����" "(1-�������; 2-�������; 3-������������; 4-�� ����) " 1 2 3 4))
  (if (eq ?response 1) then
    (bind ?fact-val beginner)
  )
  (if (eq ?response 2) then
    (bind ?fact-val student)
  )
  (if (eq ?response 3) then
    (bind ?fact-val pro)
  )
  (if (eq ?response 4) then
    (bind ?fact-val unknown)
    (print-message "�� ����������, � ���������� ������ :-)")
  )
  ?fact-val
)

; ���������� ������������, ����� � ���� ���� ����
; ���������� �������� �����, ����������� �� ��� ������
(deffunction ask-exp ()
  (bind ?response (ask-question "����� � ��� ���� ���� �� ���������?" "(1-������ 1 ����; 2-�� 1 �� 3 ���; 3-����� 3 ���) " 1 2 3))
  (if (eq ?response 1) then
    (bind ?fact-val less-1)
  )
  (if (eq ?response 2) then
    (bind ?fact-val from-1-to-3)
  )
  (if (eq ?response 3) then
    (bind ?fact-val more-3)
  )
  ?fact-val
)

; ���������� ������������, ����� � ���� �����������
; ���������� �������� �����, ����������� �� ��� ������
(deffunction ask-education ()
  (bind ?response (ask-question "���� �� � ��� ����������� �����������?" "(1-���; 2-��,�������; 3-��,������) " 1 2 3))
  (if (eq ?response 1) then
    (bind ?fact-val none)
  )
  (if (eq ?response 2) then
    (bind ?fact-val medium)
  )
  (if (eq ?response 3) then
    (bind ?fact-val high)
  )
  ?fact-val
)

; ���������� ������������, � ����� ����� ����� ������
; ���������� �������� �����, ����������� �� ��� ������
(deffunction ask-ac-jenre ()
  (bind ?response (ask-question "� ����� �����, � ��������, �� ������ ������?" "(1-���; 2-�����; 3-����; 4-�����; 5-����������� ������; 6-�� ����) " 1 2 3 4 5 6))
  (if (eq ?response 1) then
    (bind ?fact-val rock)
  )
  (if (eq ?response 2) then
    (bind ?fact-val metal)
  )
  (if (eq ?response 3) then
    (bind ?fact-val jazz)
  )
  (if (eq ?response 4) then
    (bind ?fact-val fusion)
  )
  (if (eq ?response 5) then
    (bind ?fact-val electronic)
  )
  (if (eq ?response 6) then
    (bind ?fact-val unknown)
    (print-message "��... ���������, ��� ����� �������")
  )
  ?fact-val
)

; ������ ������������ ������� ������, �������� � ������� �����
; ���������� �������� ������, ��� STRING
(deffunction ask-ac-band ()
  (bind ?response (ask-question "�������� ������, �������� � ������� ����� (�������� � ������� �������� \"\", ����� � ����� ��������� � ����������): " "" ))
  (bind ?response-str (sub-string 1 (str-length ?response) ?response))
  (while (eq (str-length ?response-str) 0) do
    (print-message "�� ������ ������ ��� ��������, �� ������ ��?")
    (bind ?response (ask-question "����������, ���������, � �� ��������� � ��������: " "" ))
    (bind ?response-str (sub-string 1 (str-length ?response) ?response))
  )
  
  ?response-str
)

; ���������� ������������, ��� �� ����� ������������ ����������� ���������
; ���������� �������� �����, ����������� �� ��� ������
(deffunction ask-el-type ()
  (bind ?response (ask-question "��� �� ������ ������������ ����������� ������� ���������?" "(1-� �������� ��������� �����������; 2-��� ���������; 3-�� ����) " 1 2 3))
  (if (eq ?response 1) then
    (bind ?fact-val drumkit)
  )
  (if (eq ?response 2) then
    (bind ?fact-val percussion)
  )
  (if (eq ?response 3) then
    (bind ?fact-val unknown)
    (print-message "� ��������� ����� ����������� �������")
  )
  ?fact-val
)

; ���������� ������������, ����� ���� ����������
; ���������� �������� �����, ����������� �� ��� ������
(deffunction ask-pr-type ()
  (bind ?response (ask-question "����� ����� ���� ����� ����������?" "(1-�������� ��������; 2-���� �� ������� ���������; 3-�� ����) " 1 2 3))
  (if (eq ?response 1) then
    (bind ?fact-val pad)
  )
  (if (eq ?response 2) then
    (bind ?fact-val kit)
  )
  (if (eq ?response 3) then
    (bind ?fact-val unknown)
  )
  ?fact-val
)

; ���������� ������������ ��� ���������
; ���������� �������� �����, ����������� �� ��� ������
(deffunction ask-pr-rudiments ()
  (bind ?response (ask-question "������ �� � ��� ���������� ����� ��������� ���, ��� ������, ������, ���������, �����?" "(1-�����, ��; 2-���; 3-� �� ����, ��� ��� �����) " 1 2 3))
  (if (eq ?response 1) then
    (bind ?fact-val yes)
  )
  (if (eq ?response 2) then
    (bind ?fact-val no)
  )
  (if (eq ?response 3) then
    (bind ?fact-val dontknow)
  )
  ?fact-val
)


; ������� �� ����� ������ ������������ ���������
; ?ids - id ������������ ���������, ��� MULTIFIELD
(deffunction print-ac-models (?ids)
  (if (eq (length$ ?ids) 1) then
    (bind ?m "�������� ��� ����� ���������:")
  else 
    (bind ?m "�������� ��� ����� ���������:")
  )
  (print-message-without-delay "��� ������ ������ ��� " ?m)
  
  (loop-for-count (?i 1 (length$ ?ids)) do
    (if (neq ?i 1) then
      (print-animated-text crlf)
    )
    (do-for-fact
      ((?drums db-acoustic_drumkit))
      (eq ?drums:id (nth$ ?i ?ids))
      (print-animated-text "        " ?drums:brand " " ?drums:model " ���������� " ?drums:price "$. (�����: " ?drums:link ")")
    )
  )
  
  (if (eq (length$ ?ids) 1) then
    (sleep 1.5)
    (print-animated-text crlf)
  else
    (print-animated-text crlf)
    (print-message "��� ��� ��������� �� ������ � ���������� ������ ��������� �������� � ������������� - ��� ������, ��� �����")
  )
  
  (return TRUE)
)

; ������� �� ����� ������ ����������� ���������
; ?ids - id ����������� ���������, ��� MULTIFIELD
(deffunction print-el-models (?ids)
  (if (eq (length$ ?ids) 1) then
    (bind ?m "�������� ��� ����� ������:")
  else 
    (bind ?m "�������� ��� ����� ������:")
  )
  (print-message-without-delay "��� ������ ������ ��� " ?m)
  
  (loop-for-count (?i 1 (length$ ?ids)) do
    (if (neq ?i 1) then
      (print-animated-text crlf)
    )
    (do-for-fact
      ((?drums db-electronic_drums))
      (eq ?drums:id (nth$ ?i ?ids))
      (print-animated-text "        " ?drums:brand " " ?drums:model " ���������� " ?drums:price "$. (�����: " ?drums:link ")")
    )
  )
  
  (if (eq (length$ ?ids) 1) then
    (sleep 1.5)
    (print-animated-text crlf)
  else
    (print-animated-text crlf)
    (print-message "��� ��� ��������� �� ������ � ���������� ������ ��������� �������� � ������������� - ��� ������, ��� �����")
  )
  
  (return TRUE)
)






;;;;;;;;;;;;;;;;;
;;; DEFRULE
;;;

;
; level 0
;

; ��������� ������ �������
; ����������� ������ ��� ������� ���� �������
(defrule rule-do-start
  (declare (salience 100))
  (initial-fact)
  (not (state ?))
  =>
  (message-hello)
  (assert (state start))
)

; ��������� ��������� �������
(defrule rule-do-stop
  (declare (salience 100))
  (state stop)
  (name ?n)
  =>
  (message-goodbye ?n)
  (halt)
)

; ��������� ������������ �������
(defrule rule-do-restart
  (declare (salience 100))
  (state restart)
  (name ?n)
  =>
  (print-message "��������������...")
  (reset)
  (assert (state start))
  (assert (name ?n))
)

; ��������� �������, ������� ���������� ��� ������������
; ����������� ������ ��� ������� ���� ������� � ����� rule-do-start
(defrule rule-ask-name
  (declare (salience 90))
  (initial-fact)
  (not (name ?))
  =>
  (assert (name (ask-name)))
)

; ���������� ������������ ��� ������
; ����������� ����� rule-ask-type
(defrule rule-ask-money
  (declare (salience 20))
  (name ?)
  (state start)
  =>
  (assert (money (ask-money)))
)

; ��������� �������, ������� ���������� � ���� �����������
(defrule rule-ask-type
  (name ?)
  (state start)
  =>
  (assert (type (ask-type)))
)



;
; level 0.1
;

; �����������, ���� ������������ ��_������_����������_�������
(defrule rule-check-stupid 
  (declare (salience 10))
  (money -1)
  ?facts <- (state ?)
  =>
  (retract ?facts)
  (assert (state stop))
)

; ��������� ������� �������� �����
; ��� �������, ��� ������ ��� ����������
(defrule rule-check-money
  (money ?m)
  =>
  (assert (money-enough (check-money ?m)))
)

; ��������� �������, ������� ������ ������ � �����������
; ��� �������, ��� ����� �� ����������
(defrule rule-ask-restart
  (money-enough no)
  =>
  (assert (state (ask-restart)))
)





;
; level 1
;

; ��������� �������, ������� ���������� � ���� ������������� ���������
; ��� �������, ��� ��� ����������� - ������������� ���������
(defrule rule-ask-pr-type
  (type practice)
  =>
  (assert (pr-type (ask-pr-type)))
)



;
; level 2
;


; ��������� �������, ������� ���������� ������������, �������� �� �� ������� ����������� ���������
; ��� �������, ��� ���� - ����������� ������
; ����������� ����� rule-ask-ac-usage
(defrule rule-ask-set-to-electronic-type
  (declare (salience 10))
  (ac-jenre electronic)
  =>
  (assert (ac-to-el (ask-yes-or-no "��� ����� electronic � ������� ��� ������� ��-���� ����������� ���������. �� ��������?" "������, ������� ����������� ���������" "")))
)

; ��������� �������, ������� ���������� �� �������������
; ��� �������, ��� ��� ����������� - ������������ ���������
(defrule rule-ask-ac-usage
  (type acoustic)
  (ac-jenre ?)
  (not (ac-jenre unknown))
  =>
  (assert (ac-usage (ask-ac-usage)))
)

; ��������� �������, ������� ���������� ������ ������������� �����
; ��� �������, ��� ��� ������������� ��������� - ����
(defrule rule-ask-pr-size
  (pr-type pad)
  =>
  (assert (pr-size (ask-2-param-question "�������� ������ ����. ������ - ������� ������������ � ����������, ������ - ����������� � �������� �������� ����� ���������." "������" "������" small normal "" "")))
)

; ��������� �������, ������� ���������� ��� ���������
; ��� �������, ��� ��� ������������� ��������� ������������ �� �����
(defrule rule-ask-pr-rudiments
  (pr-type unknown)
  =>
  (assert (pr-rudiments (ask-pr-rudiments)))
)




;
; level 2.1
;

; ������������ ������� � ������ ����������� ��������� �� ������������
; ��� �������, ��� ������������ ���������� �� ���
(defrule rule-set-to-electronic-type
  (ac-to-el yes)
  ?facts <- (type ?)
  ?facts2 <- (ac-jenre ?)
  =>
  (retract ?facts)
  (retract ?facts2)
  (assert (type electronic))
)

; ������������� �������� ������ ������������ ��������� � 0 (sub-entry)
(defrule rule-set-ac-drumkit-level-subentry
  (or
    (and
      (ac-usage training)
      (skill beginner)
    ) 
    (and
      (ac-usage training)
      (skill student)
    )
  )
  =>
  (assert (ac-drumkit-level 0))
)

; ������������� �������� ������ ������������ ��������� � 1 (entry)
(defrule rule-set-ac-drumkit-level-entry
  (or
    (and
      (ac-usage repetitions)
      (skill beginner)
    ) 
    (and
      (ac-usage repetitions)
      (skill student)
    )
  )
  =>
  (assert (ac-drumkit-level 1))
)

; ������������� �������� ������ ������������ ��������� � 2 (student)
(defrule rule-set-ac-drumkit-level-student
  (or
    (and
      (ac-usage concerts)
      (skill beginner)
    ) 
    (and
      (ac-usage repetitions)
      (skill pro)
    )
    (and
      (ac-usage training)
      (skill pro)
    )
  )
  =>
  (assert (ac-drumkit-level 2))
)

; ������������� �������� ������ ������������ ��������� � 3 (semi-pro)
(defrule rule-set-ac-drumkit-level-semipro
  (ac-usage concerts)
  (skill student)
  =>
  (assert (ac-drumkit-level 3))
)

; ������������� �������� ������ ������������ ��������� � 4 (pro)
(defrule rule-set-ac-drumkit-level-pro
  (or
    (ac-usage recording)
    (and
      (ac-usage concerts)
      (skill pro)
    )
  )
  =>
  (assert (ac-drumkit-level 4))
)

; ������������� ������� ����������� ��������� � 0 (entry)
(defrule rule-set-el-level-entry
  (type electronic)
  (skill beginner)
  =>
  (assert (el-level 0))
)

; ������������� ������� ����������� ��������� � 1 (semi-pro)
(defrule rule-set-el-level-semipro
  (type electronic)
  (skill student)
  =>
  (assert (el-level 1))
)

; ������������� ������� ����������� ��������� � 2 (pro)
(defrule rule-set-el-level-pro
  (type electronic)
  (skill pro)
  =>
  (assert (el-level 2))
)



;
; level 3
;

; ��������� �������, ������� ���������� ������������, �������� �� �� ������ ���������
; ��� �������, ��� �� ��������� ���������
(defrule rule-set-pr-type-1
  (pr-type unknown)
  (pr-rudiments yes)
  ?facts <- (pr-type ?)
  =>
  (retract ?facts)
  (print-message "������, �� ��� ������ ���������� � ���������� ���� �� ���������")
  (assert (pr-type (ask-2-param-question "�� ��������, ����� � ��������� ��� ��� ������������� ���������?" "��" "���, ��� ����� ���" kit pad "" "������ - ����� :-)")))
)

; ��������� �������, ������� ���������� ������������, �������� �� �� ������ ���
; ��� �������, ��� �� �� ��������� ���������
(defrule rule-set-pr-type-2
  (pr-type unknown)
  (pr-rudiments no)
  ?facts <- (pr-type ?)
  =>
  (retract ?facts)
  (print-message "��, ����� ��� ����� ��� ������� ���������� ��� �������� ������������ � ������ ��� ����� ������������� ���")
  (assert (pr-type (ask-2-param-question "�� ��������, ����� � ��������� ��� ��� ������������� ���?" "��" "���, ��� ����� ������ ���������" pad kit "" "������, ��� �������")))
)

; ��������� �������, ������� ���������� ������������, �������� �� �� ������ ���
; ��� �������, ��� �� �� ����� ��� ����� ���������
(defrule rule-set-pr-type-3
  (pr-type unknown)
  (pr-rudiments dontknow)
  ?facts <- (pr-type ?)
  =>
  (retract ?facts)
  (print-message "�-�-�-�, ����� �� ��������� � ���� ���� ���� �� �����. ��� ���������� ������������ ���������, �.� �������� �������� ����, ��� ������� �� ������ �� �������")
  (assert (pr-type (ask-2-param-question "�� ��������, ����� � ��������� ��� ��� ������������� ���?" "��" "���, ��� ����� ���������" pad kit "" "�� ��� ������...")))
)




;
; level <define jenre>
;

; ��������� �������, ������� ���������� � �����
; ��� �������, ��� ��� ����������� - ������������ ���������
(defrule rule-ask-ac-jenre
  (type acoustic)
  =>
  (assert (ac-jenre (ask-ac-jenre)))
)

; ��������� �������, ������� ���������� ������������ � ������
; ��� �������, ��� ������������ ���� �� �����
(defrule rule-ask-ac-band 
  (ac-jenre unknown)
  =>
  (assert (ac-band (ask-ac-band)))
  (print-message "��� ������ � ���� ���� ������... ")
)

; ���������� ����� ������
; ��� �������, ��� ���� �� �������� (��� ������������ ���������)
(defrule rule-find-band
  (declare (salience 10))
  (ac-jenre unknown)
  (ac-band ?b)
  
  (db-band (name ?b) (jenre ?j) )
  
  ?facts <- (ac-jenre ?)
  ?facts2 <- (ac-band ?)
  =>
  (retract ?facts)
  (retract ?facts2)
  (print-message "� �����! ��� � ���� - " ?j)
  (assert (ac-jenre ?j))
)

; ��������� �������, ������� ����������, ��������� �� ����� ������
; ��� �������, ��� ���� ���� �� �������� (������ �� �������)
(defrule rule-find-band-notfound
  (ac-jenre unknown)
  (ac-band ?b)
  
  ?facts <- (ac-jenre ?)
  ?facts2 <- (ac-band ?)
  =>
  (retract ?facts)
  (retract ?facts2)
  (print-message "�� ������ ����� ���� ������ \"" ?b "\"")
  (assert (ac-jenre (ask-2-param-question "�� ������ ����������� ����� ���� ������ ������ ��� �������� ���� �� ��������� (rock)?" "����� ����" "�������� rock" unknown rock "" "")))
)





;
; level <define skill>
;

; ��������� �������, ������� ���������� ������������ � ��� ������
; ��� �������, ��� ������������� - ��������� ��� ���������� (��� ������������ ���������), ��� ��� ��������� - �����������
(defrule rule-ask-skill
  (or
    (ac-usage training)
    (ac-usage repetitions)
    (ac-usage concerts)
    (and
      (el-type ?)
      (not (el-type unknown))
    )
  )
  =>
  (assert (skill (ask-skill)))
)

; ��������� �������, ������� ���������� �� �����
; ��� �������, ��� ���� ������� ������������ �� �����
; ����������� ����� rule-ask-education
(defrule rule-ask-exp
  (declare (salience 10))
  (skill unknown)
  =>
  (assert (exp (ask-exp)))
)

; ��������� �������, ������� ���������� �� �����������
; ��� �������, ��� ���� ������� ������������ �� �����
(defrule rule-ask-education
  (skill unknown)
  =>
  (assert (education (ask-education)))
)

; ������������� �������� ������ � pro
(defrule rule-set-skill-pro
  (skill unknown)
  (or
    (exp more-3)
    (and
      (exp from-1-to-3)
      (education high)
    )
  )
  ?facts <- (skill ?)
  =>
  (retract ?facts)
  (print-message "���, �� �� ��� ������������")
  (assert (skill pro))
)

; ������������� �������� ������ � student
(defrule rule-set-skill-student
  (skill unknown)
  (or
    (and
      (exp from-1-to-3)
      (education none)
    )
    (and
      (exp from-1-to-3)
      (education medium)
    )
    (and
      (exp less-1)
      (education high)
    )
  )
  ?facts <- (skill ?)
  =>
  (retract ?facts)
  (print-message "� �����, ��� �� ����� ������ ��������������")
  (assert (skill student))
)

; ������������� �������� ������ � beginner
(defrule rule-set-skill-beginner
  (skill unknown)
  (or
    (and
      (exp less-1)
      (education none)
    ) 
    (and
      (exp less-1)
      (education medium)
    )
  )
  ?facts <- (skill ?)
  =>
  (retract ?facts)
  (print-message "� �����, ��� �� ��� �������. �� �� ������������ - �� ������� :-)" )
  (assert (skill beginner))
)



;
; level <define el-drums type>
;

; ��������� �������, ������� ���������� � ���� ����������� ���������
; ��� �������, ��� ��� ����������� - ����������� ���������
(defrule rule-ask-el-type
  (type electronic)
  =>
  (assert (el-type (ask-el-type)))
)

; ��������� �������, ������� ���������� ������������, ���� �� � ���� ������ ���������
; ��� �������, ��� ������������ �� ����� ��� ����������� ���������
(defrule rule-ask-el-exist
  (el-type unknown)
  =>
  (assert (el-exist (ask-yes-or-no "���� �� � ��� ��� ������� ���������?" "" "������ �������� ��� ����������� ������� ���������, ������ ��� � ��� ��� ������")))
)

; ��������� �������, ������� ���������� ������������, �������� �� �� ������ ������������� ���������
; ��� �������, ��� � ���� ���� ������ ���������
(defrule rule-ask-el-agree
  (el-exist yes)
  =>
  (assert (el-agree (ask-yes-or-no "�� ������ ���������� ������������� ���������, ��� ���������� � ����� ��� ������������ ������� ���������. ��� ������� ����� ���� ������� ������������. �� ��������?" "������ �������� ��� ���������" "������ �������� ��� ����������� ������� ���������")))
)

; ������������� �������� ���� ����������� ��������� ��� ���������
; ��� �������, ��� ����� ��� ���� ��������� ��� ����������, � � ���� ��� ������ ��������� ��� �� �� ����� �������� ���������
(defrule rule-set-el-type-drumkit
  (and
    (not (el-type drumkit))
    (not (el-type percussion))
  )
  
  (or
    (el-exist no)
    (el-agree no)
  )
  ?facts <- (el-type ?)
  =>
  (retract ?facts)
  (assert (el-type drumkit))
)

; ������������� �������� ���� ����������� ��������� ��� ���������
; ��� �������, ��� ����� ��� ���� ��������� ��� ����������, � �� ���������� ������ ���������
(defrule rule-set-el-type-percussion
  (and
    (not (el-type drumkit))
    (not (el-type percussion))
  )
  (el-agree yes)
  ?facts <- (el-type ?)
  =>
  (retract ?facts)
  (assert (el-type percussion))
)








;
; level <db search>
;

; =acoustic drums=

; ��������� ����� ����������� ������ ������������ ��������� � ��
; ��� �������, ��� ���������� ������� � ����
(defrule rule-search-ac-model
  (type acoustic)
  (not (ac-jenre unknown))
  (money ?m)
  (ac-drumkit-level ?l)
  (ac-jenre ?s)
  =>
  (print-message "��, ��� � ������ ������, � ������. ������ ����� ���������� ������ � ���� ������...")
  (assert (ac-found-model-ids (search-ac-models ?m ?l ?s)))
)

; ��������� ����� ������� ������ ������������ ��������� � ��
; ��� �������, ��� ���������� (������� ��� �� �������) ���������� ������
(defrule rule-search-ac-model-better
  (ac-found-model-ids $?ids)
  (money ?m)
  (ac-drumkit-level ?l)
  (ac-jenre ?s)
  =>
  (assert (ac-found-model-better-id (search-ac-model-better ?ids ?m ?l ?s)))
)

; ��������� ����� ������ ������������ ���������, ������ �� �������
; ��� �������, ��� ���������� ��� ���������� ������
(defrule rule-search-ac-model-money-best
  (ac-found-model-ids $?ids)
  (ac-found-model-better-id ?id-better)
  (money ?m)
  (ac-jenre ?s)
  =>
  (assert (ac-found-model-money-best-id (search-ac-model-money-best ?ids ?id-better ?m ?s)))
)

; ������� ���������, ���� ������ �� �������
; ��� �������, ��� ��� ��������� ������ ����������
; ����������� ������ �� ���� �����������
(defrule rule-ac-model-nothing-found
  (declare (salience 60))
  (ac-found-model-ids NOTHING-FOUND)
  (ac-found-model-better-id NOTHING-FOUND)
  (ac-found-model-money-best-id NOTHING-FOUND)
  =>
  (print-message "��, ���-�� � ������ ��������������, �� ���� ������ �����... :'(")
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ������ ����������� ���������
; ����������, ��� �������� ����� ������, �.� ������ �� �������
(defrule rule-ac-model-found-best
  (declare (salience 50))
  (ac-found-model-ids $?ids)
  (ac-found-model-better-id NOTHING-FOUND)
  (ac-found-model-money-best-id NOTHING-FOUND)
  
  =>
  (print-message "���-�� �����, ���������...")
  (print-ac-models ?ids)
  (print-message "��� ����� ������ ��� ���, ��� � ���� ������������, ��� ��� ������ � �� ������������� :-)")
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ������ ������� ���������
; ����������, ����� ���������� ���� �� ������� ����� �� ����� �������
(defrule rule-ac-model-found-better-only
  (declare (salience 40))
  (ac-found-model-ids NOTHING-FOUND)
  (ac-found-model-better-id ?id-better)
  (ac-found-model-money-best-id NOTHING-FOUND)
  
  (db-acoustic_drumkit (id ?id-better) (brand ?b-better) (model ?md-better) (price ?p-better) (link ?l-better))
  =>
  (print-message "�� ���� ������ � ������ �� ����� � ������� ��� ��������� ����� � ������ ��� ����� ���������: " crlf "        " ?b-better " " ?md-better " ���������� " ?p-better "$. ���������� �� �� ������ �� ������: " ?l-better )
  (print-message "��� ����� ������� ������ �� ���, ��� � ���� � ���� ���������")
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ������ ������������ �� ������� ������
; ����������, ����� � ���� ������ ��� ������� ������ �������� ������, � ���� ������ ����
(defrule rule-ac-model-found-moneybest
  (declare (salience 30))
  (ac-found-model-ids NOTHING-FOUND)
  (ac-found-model-better-id NOTHING-FOUND)
  (ac-found-model-money-best-id ?id-best)
  
  (db-acoustic_drumkit (id ?id-best) (brand ?b-best) (model ?md-best) (price ?p-best) (link ?l-best))
  =>
  (print-message "� ���������, � ���� �� ���� �������������� ��� ��������� �� ���� ������, �� ���� ���������� ��������� ������� ����: " crlf "        " ?b-best " " ?md-best " ���������� " ?p-best "$. ���������� �� �� ������ �� ������: " ?l-best )
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ����������� � ��������, ���� �� ������
; ����������, ����� ����� ������ ��������� ������� ������, � ���� ��������� ������ �� ������, �� ������
(defrule rule-ac-model-found-best-and-better
  (declare (salience 20))
  (ac-found-model-ids $?ids)
  (ac-found-model-better-id ?id-better)
  (ac-found-model-money-best-id NOTHING-FOUND)
  
  (db-acoustic_drumkit (id ?id-better) (brand ?b-better) (model ?md-better) (price ?p-better) (link ?l-better))
  =>
  (print-message "���-�� �����, ���������...")
  (print-ac-models ?ids)
  (print-message "������, ������ ��������� ����� �� ��������� ���� �� ������, �� ���� �������: " crlf "        " ?b-better " " ?md-better " ���������� " ?p-better "$. ���������� �� �� ������ �� ������: " ?l-better )
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� � ��������, � �� �������
; ����������, ����� �� ������� ����� �� ��������� ������� ������, � ������� ������ �� ������ �������
(defrule rule-ac-model-found-better-and-moneybest
  (declare (salience 10))
  (ac-found-model-ids NOTHING-FOUND)
  (ac-found-model-better-id ?id-better)
  (ac-found-model-money-best-id ?id-best)
  
  (db-acoustic_drumkit (id ?id-better) (brand ?b-better) (model ?md-better) (price ?p-better) (link ?l-better))
  (db-acoustic_drumkit (id ?id-best) (brand ?b-best) (model ?md-best) (price ?p-best) (link ?l-best))
  =>
  (print-message "��� ������ ������ � �� ���� ������ � ������ �� ����� � ������� ��� ��������� ����� � ������ ��� ����� ���������: " crlf "        " ?b-better " " ?md-better " ���������� " ?p-better "$. ���������� �� �� ������ �� ������: " ?l-better )
  (print-message "������, � ���� ��������������� ��������� ����� ������� ������, �� �� ������, �������� �� ������������: " crlf "        " ?b-best " " ?md-best " ���������� " ?p-best "$. ���������� �� �� ������ �� ������: " ?l-best )
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ����������� � ������������ �� �������
; ����������, ����� ������������ ������� ������, � ����� �����, � ������� �� ��������� �������� ������
(defrule rule-ac-model-found-all
  (ac-found-model-ids $?ids)
  (ac-found-model-better-id ?)
  (ac-found-model-money-best-id ?id-best)
  
  (db-acoustic_drumkit (id ?id-best) (brand ?b-best) (model ?md-best) (price ?p-best) (link ?l-best))
  =>
  (print-message "���-�� �����, ���������...")
  (print-ac-models ?ids)
  (print-message "������, �� ��� ������ ������ �������� �� ���� ������: " crlf "        " ?b-best " " ?md-best " ���������� " ?p-best "$. ���������� �� �� ������ �� ������: " ?l-best )
  (print-message "�� � �����, �� ����� ������������� ����, � ��������� ���������� ������ �� ������� �������� �������, �������� :-)")
  (assert (state (ask-restart)))
)




; =electronic drums=

; ��������� ����� ����������� ������ ����������� ��������� � ��
; ��� �������, ��� ���������� ��� � �������
(defrule rule-search-el-model
  (type electronic)
  (money ?m)
  (el-type ?t)
  (el-level ?l)
  =>
  (print-message "��� � ���� ������...")
  (assert (el-found-model-ids (search-el-models ?m ?t ?l)))
)

; ��������� ����� ������� ������ ����������� ��������� � ��
; ��� �������, ��� ���������� (������� ��� �� �������) ���������� ������
(defrule rule-search-el-model-better
  (el-found-model-ids $?ids)
  (money ?m)
  (el-type ?t)
  (el-level ?l)
  =>
  (assert (el-found-model-better-id (search-el-model-better ?ids ?m ?t ?l)))
)

; ��������� ����� ������ ����������� ���������, ������ �� �������
; ��� �������, ��� ���������� ��� ���������� ������
(defrule rule-search-el-model-money-best
  (el-found-model-ids $?ids)
  (el-found-model-better-id ?id-better)
  (money ?m)
  (el-type ?t)
  =>
  (assert (el-found-model-money-best-id (search-el-model-money-best ?ids ?id-better ?m ?t)))
)

; ������� ���������, ���� ������ �� �������
; ��� �������, ��� ��� ��������� ������ ����������
; ����������� ������ �� ���� �����������
(defrule rule-el-model-nothing-found
  (declare (salience 60))
  (el-found-model-ids NOTHING-FOUND)
  (el-found-model-better-id NOTHING-FOUND)
  (el-found-model-money-best-id NOTHING-FOUND)
  =>
  (print-message "��, ���-�� � ������ ��������������, �� ���� ������ �����... :'(")
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ������ ����������� ���������
; ����������, ��� �������� ����� ������, �.� ������ �� �������
(defrule rule-el-model-found-best
  (declare (salience 50))
  (el-found-model-ids $?ids)
  (el-found-model-better-id NOTHING-FOUND)
  (el-found-model-money-best-id NOTHING-FOUND)
  
  =>
  (print-message "���-�� �����, ���������...")
  (print-el-models ?ids)
  (print-message "��� ����� ������ ��� ���, ��� � ���� ������������, ��� ��� ������ � �� ������������� :-)")
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ������ ������� ���������
; ����������, ����� ���������� ���� �� ������� ����� �� ����� �������
(defrule rule-el-model-found-better-only
  (declare (salience 40))
  (el-found-model-ids NOTHING-FOUND)
  (el-found-model-better-id ?id-better)
  (el-found-model-money-best-id NOTHING-FOUND)
  
  (db-electronic_drums (id ?id-better) (brand ?b-better) (model ?md-better) (price ?p-better) (link ?l-better))
  =>
  (print-message "�� ���� ������ � ������ �� ����� � ������� ��� ��������� ����� � ������ ��� ����� ���������: " crlf "        " ?b-better " " ?md-better " ���������� " ?p-better "$. ���������� �� �� ������ �� ������: " ?l-better )
  (print-message "��� ����� ������� ������ �� ���, ��� � ���� � ���� ���������")
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ������ ������������ �� ������� ������
; ����������, ����� � ���� ������ ��� ������� ������ �������� ������, � ���� ������ ����
(defrule rule-el-model-found-moneybest
  (declare (salience 30))
  (ac-found-model-ids NOTHING-FOUND)
  (ac-found-model-better-id NOTHING-FOUND)
  (ac-found-model-money-best-id ?id-best)
  
  (db-electronic_drums (id ?id-best) (brand ?b-best) (model ?md-best) (price ?p-best) (link ?l-best))
  =>
  (print-message "� ���������, � ���� �� ���� ��������� ��� ������ ������, �� ���� ���������� ��������� ������� ����: " crlf "        " ?b-best " " ?md-best " ���������� " ?p-best "$. ���������� �� �� ������ �� ������: " ?l-best )
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ����������� � ��������, ���� �� ������
; ����������, ����� ����� ������ ��������� ������� ������, � ���� ��������� ������ �� ������, �� ������
(defrule rule-el-model-found-best-and-better
  (declare (salience 20))
  (el-found-model-ids $?ids)
  (el-found-model-better-id ?id-better)
  (el-found-model-money-best-id NOTHING-FOUND)
  
  (db-electronic_drums (id ?id-better) (brand ?b-better) (model ?md-better) (price ?p-better) (link ?l-better))
  =>
  (print-message "���-�� �����, ���������...")
  (print-el-models ?ids)
  (print-message "������, ������ ��������� ����� �� ��������� ���� �� ������, �� ���� �������: " crlf "        " ?b-better " " ?md-better " ���������� " ?p-better "$. ���������� �� �� ������ �� ������: " ?l-better )
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� � ��������, � �� �������
; ����������, ����� �� ������� ����� �� ��������� ������� ������, � ������� ������ �� ������ �������
(defrule rule-el-model-found-better-and-moneybest
  (declare (salience 10))
  (el-found-model-ids NOTHING-FOUND)
  (el-found-model-better-id ?id-better)
  (el-found-model-money-best-id ?id-best)
  
  (db-electronic_drums (id ?id-better) (brand ?b-better) (model ?md-better) (price ?p-better) (link ?l-better))
  (db-electronic_drums (id ?id-best) (brand ?b-best) (model ?md-best) (price ?p-best) (link ?l-best))
  =>
  (print-message "��� ������ ������ � �� ���� ������ � ������ �� ����� � ������� ��� ��������� ����� � ������ ��� ����� ���������: " crlf "        " ?b-better " " ?md-better " ���������� " ?p-better "$. ���������� �� �� ������ �� ������: " ?l-better )
  (print-message "������, � ���� ��������������� ��������� ����� ������� ������, �� �� ������, �������� �� ������������: " crlf "        " ?b-best " " ?md-best " ���������� " ?p-best "$. ���������� �� �� ������ �� ������: " ?l-best )
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ����������� � ������������ �� �������
; ����������, ����� ������������ ������� ������, � ����� �����, � ������� �� ��������� �������� ������
(defrule rule-el-model-found-all
  (el-found-model-ids $?ids)
  (el-found-model-better-id ?)
  (el-found-model-money-best-id ?id-best)
  
  (db-electronic_drums (id ?id-best) (brand ?b-best) (model ?md-best) (price ?p-best) (link ?l-best))
  =>
  (print-el-models ?ids)
  (print-message "������, �� ��� ������ ������ �������� �� ���� ������: " crlf "        " ?b-best " " ?md-best " ���������� " ?p-best "$. ���������� �� �� ������ �� ������: " ?l-best )
  (print-message "�� � �����, �� ����� ������������� ����, � ��������� ���������� ������ �� ������� ��������, �������� :-)")
  (assert (state (ask-restart)))
)





; =practice kits=

; ��������� ����� ����������� � ������� ������ ������������� ��������� � ��
; ��� �������, ��� �������� ���
(defrule rule-search-pr-kit-model
  (type practice)
  (pr-type kit)
  (money ?m)
  =>
  (print-message "��� � ���� ������...")
  (assert (pr-found-kit-model-id (search-pr-drums-model ?m kit NA)))
  (assert (pr-found-kit-model-better-id (search-pr-drums-model-better ?m kit NA)))
)

; ������� ���������, ���� ������ �� �������
; ��� �������, ��� ��� ��������� ������ ����������
; ����������� ������ �� ���� �����������
(defrule rule-pr-kit-model-nothing-found
  (declare (salience 30))
  (pr-found-kit-model-id NOTHING-FOUND)
  (pr-found-kit-model-better-id NOTHING-FOUND)
  =>
  (print-message "��, ���-�� � ������ ��������������, �� ���� ������ �����... :'(")
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ������ ����������� ���������
; ����������, ��� ������ ������
(defrule rule-pr-kit-model-found-best
  (declare (salience 20))
  (pr-found-kit-model-id ?id)
  (pr-found-kit-model-better-id NOTHING-FOUND)
  
  (db-practice_drums (id ?id) (brand ?b) (model ?md) (type kit) (price ?p) (link ?l))
  =>
  (print-message "���-�� �����, ���������...")
  (print-message "�� ���� ������ ��� �������� ��� ����� ���������: " crlf "        " ?b " " ?md " ���������� " ?p "$. ���������� �� �� ������ �� ������: " ?l )
  (print-message "��� ����� ������, ��� � ���� ��� ����������, ��� ��� ������ � �� ������������� :-)")
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ������ ������� ���������
; ����������, ����� �� ������� ����� �� ����� �������
(defrule rule-pr-kit-model-found-better-only
  (declare (salience 10))
  (pr-found-kit-model-id NOTHING-FOUND)
  (pr-found-kit-model-better-id ?id-better)
  
  (db-practice_drums (id ?id-better) (brand ?b-better) (model ?md-better) (type kit) (price ?p-better) (link ?l-better))
  =>
  (print-message "�� ���� ������ � ������ �� ����� � ������� ��� ��������� ����� � ������ ��� ����� ���������: " crlf "        " ?b-better " " ?md-better " ���������� " ?p-better "$. ���������� �� �� ������ �� ������: " ?l-better )
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ��� ���������
; ����������, ����� ������� ����� �� ����������, �� ����� ��������� �� ��������
(defrule rule-pr-kit-model-found
  (pr-found-kit-model-id ?id)
  (pr-found-kit-model-better-id ?id-better)
  
  (db-practice_drums (id ?id) (brand ?b) (model ?md) (type kit) (price ?p) (link ?l))
  (db-practice_drums (id ?id-better) (brand ?b-better) (model ?md-better) (type kit) (price ?p-better) (link ?l-better))
  =>
  (print-message "���-�� �����, ���������...")
  (print-message "�� ���� ������ ��� �������� ��� ����� ���������: " crlf "        " ?b " " ?md " ���������� " ?p "$. ���������� �� �� ������ �� ������: " ?l )
  (print-message "������, �� ��� ������ ������� ��������� ����� � ������ ����� ���������: " crlf "        " ?b-better " " ?md-better " ���������� " ?p-better "$. ���������� �� �� ������ �� ������: " ?l-better )
  (assert (state (ask-restart)))
)



; =practice pads=

; ��������� ����� ����������� � ������� ������ ������������� ����� � ��
; ��� �������, ��� �������� ��� � ������
(defrule rule-search-pr-pad-model
  (type practice)
  (pr-type pad)
  (pr-size ?s)
  (money ?m)
  =>
  (print-message "��� � ���� ������...")
  (assert (pr-found-pad-model-id (search-pr-drums-model ?m pad ?s)))
  (assert (pr-found-pad-model-better-id (search-pr-drums-model-better ?m pad ?s)))
)

; ������� ���������, ���� ������ �� �������
; ��� �������, ��� ��� ��������� ������ ����������
; ����������� ������ �� ���� �����������
(defrule rule-pr-pad-model-nothing-found
  (declare (salience 30))
  (pr-found-pad-model-id NOTHING-FOUND)
  (pr-found-pad-model-better-id NOTHING-FOUND)
  =>
  (print-message "��, ���-�� � ������ ��������������, �� ���� ������ �����... :'(")
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ������ ����������� ������
; ����������, ��� ������ ������
(defrule rule-pr-pad-model-found-best
  (declare (salience 20))
  (pr-found-pad-model-id ?id)
  (pr-found-pad-model-better-id NOTHING-FOUND)
  
  (db-practice_drums (id ?id) (brand ?b) (model ?md) (type pad) (price ?p) (link ?l))
  =>
  (print-message "���-�� �����, ���������...")
  (print-message "�� ���� ������ ��� �������� ��� ����� ���: " crlf "        " ?b " " ?md " ���������� " ?p "$. ���������� �� ���� ������ �� ������: " ?l )
  (print-message "��� ����� ������, ��� � ���� ��� ����������, ��� ��� ������ � �� ������������� :-)")
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ������ ������� ������
; ����������, ����� �� ������� ����� �� ����� �������
(defrule rule-pr-pad-model-found-better-only
  (declare (salience 10))
  (pr-found-pad-model-id NOTHING-FOUND)
  (pr-found-pad-model-better-id ?id-better)
  
  (db-practice_drums (id ?id-better) (brand ?b-better) (model ?md-better) (type pad) (price ?p-better) (link ?l-better))
  =>
  (print-message "�� ���� ������ � ������ �� ����� � ������� ��� ��������� ����� � ������ ��� ����� ���: " crlf "        " ?b-better " " ?md-better " ���������� " ?p-better "$. ���������� �� ���� ������ �� ������: " ?l-better )
  (assert (state (ask-restart)))
)

; ������� ���������, ���� ������� ��� ������
; ����������, ����� ������� ����� �� ����������, �� ����� ��������� �� ��������
(defrule rule-pr-pad-model-found
  (pr-found-pad-model-id ?id)
  (pr-found-pad-model-better-id ?id-better)
  
  (db-practice_drums (id ?id) (brand ?b) (model ?md) (type pad) (price ?p) (link ?l))
  (db-practice_drums (id ?id-better) (brand ?b-better) (model ?md-better) (type pad) (price ?p-better) (link ?l-better))
  =>
  (print-message "���-�� �����, ���������...")
  (print-message "�� ���� ������ ��� �������� ��� ����� ���: " crlf "        " ?b " " ?md " ���������� " ?p "$. ���������� �� ���� ������ �� ������: " ?l )
  (print-message "������, �� ��� ������ ������� ��������� ����� � ������ ����� ���: " crlf "        " ?b-better " " ?md-better " ���������� " ?p-better "$. ���������� �� ���� ������ �� ������: " ?l-better )
  (assert (state (ask-restart)))
)

