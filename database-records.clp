;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; ���������� ���� ������ ��� ���������� ������� "����� ������� ���������"
;;; �������� ������ ��������������: Sonor, Mapex, Roland, Stagg
;;;
;;;
;;; ������:  0.1.0.20
;;; �����:   ������ �����
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;






;;;;;;;;;;;;;;;;;
;;; DEFFACTS
;;;

(deffacts bands
  (db-band (name "metallica") (jenre metal))
  (db-band (name "ayreon") (jenre metal))
  (db-band (name "therion") (jenre metal))
  (db-band (name "nightwish") (jenre metal))
  (db-band (name "slayer") (jenre metal))
  (db-band (name "deep purple") (jenre rock))
  (db-band (name "pink floyd") (jenre rock))
  (db-band (name "uriah heep") (jenre rock))
  (db-band (name "rainbow") (jenre rock))
  (db-band (name "led zeppelin") (jenre rock))
  (db-band (name "apple tea") (jenre jazz))
  (db-band (name "simply red") (jenre jazz))
  (db-band (name "fourplay") (jenre jazz))
  (db-band (name "material") (jenre jazz))
  (db-band (name "mike stern") (jenre fusion))
  (db-band (name "depeche mode") (jenre electronic))
)

(deffacts acoustic_drumkits
  (db-acoustic_drumkit (id 1) (brand "Sonor") (model "F507 Studio 1") (level 0) (snaredrum 14-5.5) (bassdrum 20) (floortom 14) (toms 10-12) (price 650) (link "http://www.sonor.ru/force507-studio1.htm") )
  (db-acoustic_drumkit (id 2) (brand "Sonor") (model "F507 Stage 1") (level 0) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 12-13) (price 650) (link "http://www.sonor.ru/force507-stage1.htm") )
  (db-acoustic_drumkit (id 3) (brand "Sonor") (model "F507 Combo") (level 0) (snaredrum 14-5.5) (bassdrum 18) (floortom 14) (toms 10-12) (price 650) (link "http://www.sonor.ru/force507-combo.htm") )
  
  (db-acoustic_drumkit (id 4) (brand "Sonor") (model "F1007 Stage 2") (level 1) (snaredrum 14-5.5) (bassdrum 22) (floortom 14) (toms 10-12) (price 700) (link "http://www.sonor.ru/force1007-stage2.htm") )
  (db-acoustic_drumkit (id 5) (brand "Sonor") (model "F1007 Stage 1") (level 1) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 12-13) (price 710) (link "http://www.sonor.ru/force1007-stage1.htm") )
  (db-acoustic_drumkit (id 6) (brand "Sonor") (model "F1007 Studio 1") (level 1) (snaredrum 14-5.5) (bassdrum 22) (floortom 14) (toms 10-12) (price 720) (link "http://www.sonor.ru/force1007-studio1.htm") )
  
  (db-acoustic_drumkit (id 7) (brand "Sonor") (model "F2007 Stage 1") (level 2) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 12-13) (price 960) (link "http://www.sonor.ru/force2007-stage1.htm") )
  (db-acoustic_drumkit (id 8) (brand "Sonor") (model "F2007 Stage 2") (level 2) (snaredrum 14-5.5) (bassdrum 22) (floortom 14) (toms 10-12) (price 1250) (link "http://www.sonor.ru/force2007-stage2.htm") )
  (db-acoustic_drumkit (id 9) (brand "Sonor") (model "F2007 Stage 2") (level 2) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 10-12) (price 1250) (link "http://www.sonor.ru/force2007-stage3.htm") )
  (db-acoustic_drumkit (id 10) (brand "Sonor") (model "F2007 Stage 2") (level 2) (snaredrum 14-5.5) (bassdrum 20) (floortom 14) (toms 10-12) (price 1250) (link "http://www.sonor.ru/force2007-studio1.htm") )
  
  (db-acoustic_drumkit (id 11) (brand "Sonor") (model "F3007 Stage 1") (level 3) (snaredrum 14-5.5) (bassdrum 20) (floortom 16) (toms 12-13) (price 1600) (link "http://www.sonor.ru/force3007-stage1.htm") )
  (db-acoustic_drumkit (id 12) (brand "Sonor") (model "F3007 Stage 2") (level 3) (snaredrum 14-5.5) (bassdrum 22) (floortom 14) (toms 10-12) (price 1700) (link "http://www.sonor.ru/force3007-stage2.htm") )
  (db-acoustic_drumkit (id 13) (brand "Sonor") (model "F3007 Stage 3") (level 3) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 10-12) (price 1700) (link "http://www.sonor.ru/force3007-stage3.htm") )
  (db-acoustic_drumkit (id 14) (brand "Sonor") (model "F3007 Studio 1") (level 3) (snaredrum 14-5.5) (bassdrum 22) (floortom 14) (toms 10-12) (price 1750) (link "http://www.sonor.ru/force3007-studio1.htm") )
  
  (db-acoustic_drumkit (id 15) (brand "Sonor") (model "SC Studio 1 Shell") (level 4) (snaredrum 14-5.5) (bassdrum 22) (floortom 14) (toms 10-12) (price 3000) (link "http://www.sonor.ru/classix-studio1.htm") )
  (db-acoustic_drumkit (id 16) (brand "Sonor") (model "SC Stage 1 Shell")  (level 4) (snaredrum 14-5) (bassdrum 22) (floortom 16) (toms 12-13) (price 3000) (link "http://www.sonor.ru/classix-stage1.htm") )
  (db-acoustic_drumkit (id 17) (brand "Sonor") (model "SC Stage 2")        (level 4) (snaredrum 14-5) (bassdrum 22) (floortom 16) (toms 10-12) (price 3000) (link "http://www.sonor.ru/classix-stage2.htm") )
  (db-acoustic_drumkit (id 18) (brand "Sonor") (model "SC Stage 3")        (level 4) (snaredrum 14-5) (bassdrum 22) (floortom 16) (toms 10-12) (price 3000) (link "http://www.sonor.ru/classix-stage3.htm") )
  (db-acoustic_drumkit (id 19) (brand "Sonor") (model "SC Rock")           (level 4) (snaredrum 14-5) (bassdrum 24) (floortom 14-16) (toms 12) (price 3000) (link "http://www.sonor.ru/classix-rock.htm") )
  
  (db-acoustic_drumkit (id 20) (brand "Sonor") (model "DL Stage 1 Shell")  (level 4) (snaredrum 14-5) (bassdrum 22) (floortom 16) (toms 12-13) (price 3000) (link "http://www.sonor.ru/delite-07-stage1.htm") )
  (db-acoustic_drumkit (id 21) (brand "Sonor") (model "DL Stage 2 Shell")  (level 4) (snaredrum 14-5) (bassdrum 22) (floortom 14) (toms 10-12) (price 3000) (link "http://www.sonor.ru/delite-07-stage2.htm") )
  (db-acoustic_drumkit (id 22) (brand "Sonor") (model "DL Studio 1 Shell") (level 4) (snaredrum 14-5) (bassdrum 22) (floortom 14) (toms 10-12) (price 3000) (link "http://www.sonor.ru/delite-07-studio.htm") )
  
  (db-acoustic_drumkit (id 23) (brand "Mapex") (model "TN5254MC") (level 0) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 12-13) (price 570) (link "http://www.mapex.ru/drumsets/tornado/tn5254c.htm") )
  (db-acoustic_drumkit (id 24) (brand "Mapex") (model "VR5254") (level 0) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 12-13) (price 650) (link "http://www.mapex.ru/drumsets/voyager/voyager.html") )
  (db-acoustic_drumkit (id 25) (brand "Mapex") (model "VR5254TCZ") (level 0) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 10-12) (price 836) (link "http://www.mapex.ru/drumsets/voyager/voyager.html") ) ; ������� �������
  
  (db-acoustic_drumkit (id 26) (brand "Mapex") (model "HX5255T") (level 1) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 12-13) (price 850) (link "http://www.mapex.ru/drumsets/horizon_hx/horizon_hx.html") )
  (db-acoustic_drumkit (id 27) (brand "Mapex") (model "HZB5255T") (level 1) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 12-13) (price 1030) (link "http://www.mapex.ru/drumsets/horizon_hzb/horizon_hzb.html") )
  
  (db-acoustic_drumkit (id 28) (brand "Mapex") (model "MR5045T") (level 2) (snaredrum 14-5.5) (bassdrum 20) (floortom 14) (toms 10-12) (price 1340) (link "http://www.mapex.ru/drumsets/meridian_birch/MR5045T.html") )
  (db-acoustic_drumkit (id 29) (brand "Mapex") (model "MR5245T") (level 2) (snaredrum 14-5.5) (bassdrum 22) (floortom 14) (toms 10-12) (price 1380) (link "http://www.mapex.ru/drumsets/meridian_birch/MR5245T.html") )
  (db-acoustic_drumkit (id 30) (brand "Mapex") (model "MR5255TA") (level 2) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 12-13) (price 1380) (link "http://www.mapex.ru/drumsets/meridian_birch/MR5255TA.html") )
  (db-acoustic_drumkit (id 31) (brand "Mapex") (model "MR6285F") (level 2) (snaredrum 14-5.5) (bassdrum 22) (floortom 14-16) (toms 10-12) (price 1540) (link "http://www.mapex.ru/drumsets/meridian_birch/MR6285F.html") )
  
  (db-acoustic_drumkit (id 32) (brand "Mapex") (model "MP5045") (level 3) (snaredrum 14-5.5) (bassdrum 20) (floortom 14) (toms 10-12) (price 1600) (link "http://www.mapex.ru/drumsets/meridian_maple/MP5045.html") )
  (db-acoustic_drumkit (id 33) (brand "Mapex") (model "MP5245") (level 3) (snaredrum 14-5.5) (bassdrum 22) (floortom 14) (toms 10-12) (price 1600) (link "http://www.mapex.ru/drumsets/meridian_maple/MP5245.html") )
  (db-acoustic_drumkit (id 34) (brand "Mapex") (model "MP5255") (level 3) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 12-13) (price 1600) (link "http://www.mapex.ru/drumsets/meridian_maple/MP5255.html") )
  (db-acoustic_drumkit (id 35) (brand "Mapex") (model "MP6285F") (level 3) (snaredrum 14-5.5) (bassdrum 22) (floortom 14-16) (toms 10-12) (price 1970) (link "http://www.mapex.ru/drumsets/meridian_maple/MP6285F.html") )
  
  (db-acoustic_drumkit (id 36) (brand "Mapex") (model "STUDIO-SW6225A") (level 3) (snaredrum 14-5.5) (bassdrum 22) (floortom 14-16) (toms 10-12) (price 3000) (link "http://www.mapex.ru/drumsets/saturn/SW6225A.html") )
  (db-acoustic_drumkit (id 37) (brand "Mapex") (model "MICRO-SW5875A") (level 3) (snaredrum 13-5.5) (bassdrum 22) (floortom -) (toms 10-12-14) (price 3100) (link "http://www.mapex.ru/drumsets/saturn/SW5875A.html") )
  (db-acoustic_drumkit (id 38) (brand "Mapex") (model "ROCK-SW5466A") (level 3) (snaredrum 14-6.5) (bassdrum 22) (floortom 16-18) (toms 13) (price 3350) (link "http://www.mapex.ru/drumsets/saturn/SW5466A.html") )
  (db-acoustic_drumkit (id 39) (brand "Mapex") (model "STANDARD-SW5255A") (level 3) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 12-13) (price 2600) (link "http://www.mapex.ru/drumsets/saturn/SW5255A.html") )
  (db-acoustic_drumkit (id 40) (brand "Mapex") (model "JAZZ-SW5045A") (level 3) (snaredrum 14-5.5) (bassdrum 20) (floortom 14) (toms 10-12) (price 3100) (link "http://mapex.ru/drumsets/saturn/SW5045A.html") )
  (db-acoustic_drumkit (id 41) (brand "Mapex") (model "FUSION-SW5225A") (level 3) (snaredrum 14-5.5) (bassdrum 22) (floortom -) (toms 10-12-14) (price 3100) (link "http://mapex.ru/drumsets/saturn/SW5225A.html") )
  (db-acoustic_drumkit (id 42) (brand "Mapex") (model "FUSIONEASE-SW5245A") (level 3) (snaredrum 14-5.5) (bassdrum 22) (floortom 14) (toms 10-12) (price 3100) (link "http://mapex.ru/drumsets/saturn/SW5245A.html") )
  (db-acoustic_drumkit (id 43) (brand "Mapex") (model "MANHATTAN-SW4815A") (level 3) (snaredrum 14-5.5) (bassdrum 18) (floortom 14) (toms 12) (price 2950) (link "http://mapex.ru/drumsets/saturn/SW4815A.html") )
  
  (db-acoustic_drumkit (id 44) (brand "Mapex") (model "STUDIO-BM6225A") (level 4) (snaredrum 14-5.5) (bassdrum 22) (floortom -) (toms 10-12-14-16) (price 6100) (link "http://mapex.ru/drumsets/orion/BM6225A.html") )
  (db-acoustic_drumkit (id 45) (brand "Mapex") (model "STANDARD-BM5255A") (level 4) (snaredrum 14-5.5) (bassdrum 22) (floortom 16) (toms 12-13) (price 5500) (link "http://mapex.ru/drumsets/orion/BM5255A.html") )
  (db-acoustic_drumkit (id 46) (brand "Mapex") (model "FUSION-BM5225A") (level 4) (snaredrum 14-5.5) (bassdrum 22) (floortom -) (toms 10-12-14-16) (price 5500) (link "http://mapex.ru/drumsets/orion/BM5225A.html") )
)

(deffacts electronic_drums
  (db-electronic_drums (id 1) (brand "Roland") (model "TD-4K") (type drumkit) (level 0) (price 1450) (link "http://www.roland.com/products/en/TD-4K/") )
  (db-electronic_drums (id 2) (brand "Roland") (model "TD-4KX") (type drumkit) (level 0) (price 1900) (link "http://www.roland.com/products/en/TD-4KX/") )
  (db-electronic_drums (id 3) (brand "Roland") (model "TD-9K") (type drumkit) (level 0) (price 2100) (link "http://www.roland.com/products/en/TD-9K/") )
  (db-electronic_drums (id 4) (brand "Roland") (model "TD-9KX") (type drumkit) (level 0) (price 2950) (link "http://www.roland.com/products/en/TD-9KX/") )
  (db-electronic_drums (id 5) (brand "Roland") (model "TD-12KX") (type drumkit) (level 1) (price 4500) (link "http://www.roland.com/products/en/TD-12KX/") )
  (db-electronic_drums (id 6) (brand "Roland") (model "TD-20KX") (type drumkit) (level 2) (price 8500) (link "http://www.roland.com/products/en/TD-20KX/") )
  (db-electronic_drums (id 7) (brand "Roland") (model "SPD-S") (type percussion) (level 1) (price 750) (link "http://www.roland.com/products/en/SPD-S/") )
  (db-electronic_drums (id 8) (brand "Roland") (model "OCTAPAD SPD-30") (type percussion) (level 2) (price 1150) (link "http://www.roland.com/products/en/SPD-30/") )
)

(deffacts practice_drums
  (db-practice_drums (id 1) (brand "Stagg") (model "TDS-7") (type kit) (size NA) (price 290) (link "http://www.staggmusic.com/products/products_detail.php?langue=uk&oneid=1472") )
  (db-practice_drums (id 2) (brand "Stagg") (model "TDS-8") (type kit) (size NA) (price 340) (link "http://www.staggmusic.com/products/products_detail.php?langue=uk&oneid=1473") )
  (db-practice_drums (id 3) (brand "Stagg") (model "TDS-8R") (type kit) (size NA) (price 360) (link "http://www.staggmusic.com/products/products_detail.php?langue=uk&oneid=1474") )
  (db-practice_drums (id 4) (brand "Stagg") (model "TD-06.2") (type pad) (size small) (price 27) (link "http://www.staggmusic.com/products/products_detail.php?langue=uk&oneid=1479") )
  (db-practice_drums (id 5) (brand "Stagg") (model "TD-12.2") (type pad) (size normal) (price 45) (link "http://www.staggmusic.com/products/products_detail.php?langue=uk&oneid=1480") )
)
