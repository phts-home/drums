;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; ��������� ���� ������ ��� ���������� ������� "����� ������� ���������"
;;; �������� ��������� �������, � ����� ������� ��� �������������� � �������
;;;
;;; ��� ������ CLIPS - 6.3
;;;
;;;
;;; ������:  0.1.0.20
;;; �����:   ������ �����
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;






;;;;;;;;;;;;;;;;;
;;; DEFTEMPLATE
;;;

; ��������� ������ �� ��� ����������� ������
(deftemplate db-band
  (slot name (type STRING) )            ; �������� ������
  (slot jenre)                          ; ����
)

; ��������� ������ �� ��� ������ ������������ ������� ���������
(deftemplate db-acoustic_drumkit
  (slot id (type INTEGER) )             ; ���������� ����
  (slot brand (type STRING) )           ; �����
  (slot model (type STRING) )           ; ������
  (slot level (type INTEGER) )          ; ������� ��������� (0, 1, 2, 3, 4)
  (slot snaredrum)                      ; ������ ������ �������� (�������-�������)
  (slot bassdrum)                       ; ������ ���-�������� (�������)
  (slot floortom)                       ; ������� ��������� ����� (�������1-�������2 ...) (- ���� ���)
  (slot toms)                           ; ������� �������� ����� (�������1-�������2-...)
  (slot price (type NUMBER) )           ; ���������
  (slot link (type STRING) )            ; ������ �� ������ � ���������
)

; ��������� ������ �� ��� ������ ����������� ���������
(deftemplate db-electronic_drums
  (slot id (type INTEGER) )             ; ���������� ����
  (slot brand (type STRING) )           ; �����
  (slot model (type STRING) )           ; ������
  (slot type)                           ; ��� (drumkit, percussion)
  (slot level (type INTEGER) )          ; ������� (0, 1, 2)
  (slot price (type NUMBER) )           ; ���������
  (slot link (type STRING) )            ; ������ �� ������ � ���������
)

; ��������� ������ �� ��� ������ ������������� ���������
(deftemplate db-practice_drums
  (slot id (type INTEGER) )             ; ���������� ����
  (slot brand (type STRING) )           ; �����
  (slot model (type STRING) )           ; ������
  (slot type)                           ; ��� (kit, pad)
  (slot size)                           ; ������ ���� (������ ��� ���� pad, ��� kit - NA) (small, normal, NA)
  (slot price (type NUMBER) )           ; ���������
  (slot link (type STRING) )            ; ������ �� ������ � ���������
)






;;;;;;;;;;;;;;;;;
;;; DEFFUNCTION
;;;

; ���������� ���������� ������� ����� ��������� ������������ ��������� ��� ������� �����
; ?jenre - ����
; ���������� multifield-��������
(deffunction get-snaredrums (?jenre)
  (switch ?jenre
    (case metal then
      (bind ?ret (create$ 14-5.5 14-5 14-6 14-6.5))
    )
    (case jazz then
      (bind ?ret (create$ 14-5.5 14-5 14-6 14-6.5 13-5.5 13-5 13-6 13-6.5))
    )
    (case fusion then
      (bind ?ret (create$ 14-5.5 14-5 14-6 14-6.5))
    )
    (default then   ;; ��� rock
      (bind ?ret (create$ 14-5.5 14-5 14-6 14-6.5))
    )
  )
  ?ret
)

; ���������� ���������� ������� ���-��������� ������������ ��������� ��� ������� �����
; ?jenre - ����
; ���������� multifield-��������
(deffunction get-bassdrums (?jenre)
  (switch ?jenre
    (case metal then
      (bind ?ret (create$ 20 22 24))
    )
    (case jazz then
      (bind ?ret (create$ 18 20))
    )
    (case fusion then
      (bind ?ret (create$ 18 20 22))
    )
    (default then   ;; ��� rock
      (bind ?ret (create$ 20 22))
    )
  )
  ?ret
)

; ���������� ���������� ������� ����� ������������ ��������� ��� ������� �����
; ?jenre - ����
; ���������� multifield-��������
(deffunction get-toms (?jenre)
  (switch ?jenre
    (case metal then
      (bind ?ret (create$ 12 12-13 10-12-14  10-12-14-16))
    )
    (case jazz then
      (bind ?ret (create$ 10-12 10 12))
    )
    (case fusion then
      (bind ?ret (create$ 10-12 10 12 10-12-14-16))
    )
    (default then   ;; ��� rock
      (bind ?ret (create$ 12 12-13 10-12-14 10-12-14-16))
    )
  )
  ?ret
)

; ���������� ���������� ������� ��������� ����� ������������ ��������� ��� ������� �����
; ?jenre - ����
; ���������� multifield-��������
(deffunction get-floortoms (?jenre)
  (switch ?jenre
    (case metal then
      (bind ?ret (create$ - 16 18 16-18 14-16))
    )
    (case jazz then
      (bind ?ret (create$ - 14))
    )
    (case fusion then
      (bind ?ret (create$ - 14 14-16 16))
    )
    (default then   ;; ��� rock
      (bind ?ret (create$ - 16 18 16-18 14-16))
    )
  )
  ?ret
)

; ���� � ���� ������ ������ ������������ ���������, ��������� ������� ���������� � ������ ����� ������������, ���� �� ������ ��� �� ������� ����
; ?money - ������ ������������
; ?level - ������� ������
; ?jenre - ����
; ���������� id ��������� �������, ��� MULTIFIELD, ��� ������ NOTHING-FOUND, ���� ������ �� �������
(deffunction search-ac-models (?money ?level ?jenre)
  (bind ?found-model-id NOTHING-FOUND)
  (bind ?mf (create$))
  
  (bind ?snaredrums (get-snaredrums ?jenre))
  (bind ?bassdrums (get-bassdrums ?jenre))
  (bind ?toms (get-toms ?jenre))
  (bind ?floortoms (get-floortoms ?jenre))
  
  (while (and (eq (length$ ?mf) 0) (< ?level 10) ) do
    (do-for-all-facts
      ((?drums db-acoustic_drumkit))
      (and (neq (member$ ?drums:snaredrum ?snaredrums) FALSE) 
          (neq (member$ ?drums:bassdrum ?bassdrums) FALSE) 
          (neq (member$ ?drums:toms ?toms) FALSE) 
          (neq (member$ ?drums:floortom ?floortoms) FALSE) 
          (eq (member$ ?drums:id ?mf) FALSE) 
          (eq ?drums:level ?level) 
          (<= ?drums:price ?money) 
          )
      (bind ?mf (insert$ ?mf 1 ?drums:id))
    )
    (bind ?level (+ ?level 1))
  )
  
  (if (neq (length$ ?mf) 0) then 
    (bind ?found-model-id ?mf)
  )
  
  ?found-model-id
)

; ���� � ���� ������ ������ ������������ ���������, ��������� ������� ����������� � ������ ��� ������ ������������, ���� �� ������ ��� ������ ������� ������
; ?model-ids - ����� ��������� ������ ���������; ���� NOTHING-FOUND, �� ���� ����� ������� ������ ������� ������
; ?money - ������ ������������
; ?level - ������� ������
; ?jenre - ����
; ���������� id ��������� ������, ��� INTEGER, ��� ������ NOTHING-FOUND, ���� ������ �� �������
(deffunction search-ac-model-better (?model-ids ?money ?level ?jenre)
  (bind ?found-model-id NOTHING-FOUND)
  (bind ?min 999999)
  
  (bind ?snaredrums (get-snaredrums ?jenre))
  (bind ?bassdrums (get-bassdrums ?jenre))
  (bind ?toms (get-toms ?jenre))
  (bind ?floortoms (get-floortoms ?jenre))
  
  (if (eq ?model-ids NOTHING-FOUND) then
    (do-for-all-facts
      ((?drums db-acoustic_drumkit))
      (and (neq (member$ ?drums:snaredrum ?snaredrums) FALSE) 
          (neq (member$ ?drums:bassdrum ?bassdrums) FALSE) 
          (neq (member$ ?drums:toms ?toms) FALSE) 
          (neq (member$ ?drums:floortom ?floortoms) FALSE)
          (>= ?drums:level ?level) 
          (> ?drums:price ?money) 
          (< ?drums:price ?min))
      (bind ?min ?drums:price)
      (bind ?found-model-id ?drums:id)
    )
  else
    (do-for-all-facts
      ((?drums db-acoustic_drumkit))
      (and (neq (member$ ?drums:snaredrum ?snaredrums) FALSE) 
          (neq (member$ ?drums:bassdrum ?bassdrums) FALSE) 
          (neq (member$ ?drums:toms ?toms) FALSE) 
          (neq (member$ ?drums:floortom ?floortoms) FALSE)
          (eq ?drums:level ?level) 
          (> ?drums:price ?money) 
          (< ?drums:price ?min))
      (bind ?min ?drums:price)
      (bind ?found-model-id ?drums:id)
    )
  )
  
  ?found-model-id
)

; ���� � ���� ������ ������ ������������ ���������, ��������� ������� ���������� � ������ ����� ������������, ������ ������
; ?model-ids - id ����������� �������
; ?better-model-id - id ������� ������
; ?money - ������ ������������
; jenre - ����
; ���������� id ��������� ������, ��� INTEGER, ��� ������ NOTHING-FOUND, ���� ������ �� ������� ��� ��������� � ?model-id ��� ?better-model-id
(deffunction search-ac-model-money-best (?model-ids ?better-model-id ?money ?jenre)
  (bind ?found-model-id NOTHING-FOUND)
  (bind ?max 0)
  
  (bind ?snaredrums (get-snaredrums ?jenre))
  (bind ?bassdrums (get-bassdrums ?jenre))
  (bind ?toms (get-toms ?jenre))
  (bind ?floortoms (get-floortoms ?jenre))
  
  (do-for-all-facts
    ((?drums db-acoustic_drumkit))
    (and (neq (member$ ?drums:snaredrum ?snaredrums) FALSE) 
          (neq (member$ ?drums:bassdrum ?bassdrums) FALSE) 
          (neq (member$ ?drums:toms ?toms) FALSE) 
          (neq (member$ ?drums:floortom ?floortoms) FALSE)
          (<= ?drums:price ?money) 
          (>= ?drums:price ?max))
    (bind ?max ?drums:price)
    (bind ?found-model-id ?drums:id)
  )
  
  (if (or (and (neq ?model-ids NOTHING-FOUND) (member$ ?found-model-id ?model-ids) ) (and (neq ?better-model-id NOTHING-FOUND)(eq ?found-model-id ?better-model-id)) ) then
    (bind ?found-model-id NOTHING-FOUND)
  )
  
  ?found-model-id
)


; ���� � ���� ������ ������ ����������� ���������, ��������� ������� ���������� � ������ ����� ������������, ���� �� ������ ��� �� ������� ����
; ?money - ������ ������������
; ?type - ��� ������ (drumkit, percussion)
; ?level - ������� ������
; ���������� id ��������� �������, ��� MULTIFIELD, ��� ������ NOTHING-FOUND, ���� ������ �� �������
(deffunction search-el-models (?money ?type ?level)
  (bind ?found-model-id NOTHING-FOUND)
  (bind ?mf (create$))
  
  (while (and (eq (length$ ?mf) 0) (< ?level 10) ) do
    (do-for-all-facts
      ((?drums db-electronic_drums))
      (and (eq ?drums:type ?type) (eq ?drums:level ?level) (<= ?drums:price ?money) (eq (member$ ?drums:id ?mf) FALSE) ) ;(>= ?drums:price ?max)
      (bind ?mf (insert$ ?mf 1 ?drums:id))
    )
    (bind ?level (+ ?level 1))
  )
  
  (if (neq (length$ ?mf) 0) then 
    (bind ?found-model-id ?mf)
  )
  
  ?found-model-id
)

; ���� � ���� ������ ������ ����������� ���������, ��������� ������� ����������� � ������ ��� ������ ������������, ���� �� ������ ��� ������ ������� ������
; ?model-ids - ����� ��������� ������ ���������; ���� NOTHING-FOUND, �� ���� ����� ������� ������ ������� ������
; ?money - ������ ������������
; ?type - ��� ������ (drumkit, percussion)
; ?level - ������� ������
; ���������� id ��������� ������, ��� INTEGER, ��� ������ NOTHING-FOUND, ���� ������ �� �������
(deffunction search-el-model-better (?model-ids ?money ?type ?level)
  (bind ?found-model-id NOTHING-FOUND)
  (bind ?min 999999)
  
  (if (eq ?model-ids NOTHING-FOUND) then
    (do-for-all-facts
      ((?drums db-electronic_drums))
      (and (eq ?drums:type ?type) (>= ?drums:level ?level) (> ?drums:price ?money) (< ?drums:price ?min))
      (bind ?min ?drums:price)
      (bind ?found-model-id ?drums:id)
    )
  else
    (do-for-all-facts
      ((?drums db-electronic_drums))
      (and (eq ?drums:type ?type) (eq ?drums:level ?level) (> ?drums:price ?money) (< ?drums:price ?min))
      (bind ?min ?drums:price)
      (bind ?found-model-id ?drums:id)
    )
  )
  
  ?found-model-id
)

; ���� � ���� ������ ������ ����������� ���������, ��������� ������� ���������� � ������ ����� ������������, ������ ������
; ?model-ids - id ����������� �������
; ?better-model-id - id ������� ������
; ?money - ������ ������������
; ?type - ��� ����������� (drumkit, percussion)
; ���������� id ��������� ������, ��� INTEGER, ��� ������ NOTHING-FOUND, ���� ������ �� ������� ��� ��������� � ?model-id ��� ?better-model-id
(deffunction search-el-model-money-best (?model-ids ?better-model-id ?money ?type)
  (bind ?found-model-id NOTHING-FOUND)
  (bind ?max 0)
  
  (do-for-all-facts
    ((?drums db-electronic_drums))
    (and (eq ?drums:type ?type) (<= ?drums:price ?money) (>= ?drums:price ?max))
    (bind ?max ?drums:price)
    (bind ?found-model-id ?drums:id)
  )
  
  (if (or (and (neq ?model-ids NOTHING-FOUND) (member$ ?found-model-id ?model-ids) ) (and (neq ?better-model-id NOTHING-FOUND)(eq ?found-model-id ?better-model-id)) ) then
    (bind ?found-model-id NOTHING-FOUND)
  )
  
  ?found-model-id
)

; ���� � ���� ������ ������ ������������� ��������� (��� ��� ���������), ��������� ������� ���������� � ������ ����� ������������
; ?money - ������ ������������
; ?type - ��� ����������� (pad, kit)
; ?size - ������ ���� (small, normal), ��� kit - ������ NA
; ���������� id ��������� ������, ��� INTEGER, ��� ������ NOTHING-FOUND, ���� ������ �� �������
(deffunction search-pr-drums-model (?money ?type ?size)
  (bind ?found-model-id NOTHING-FOUND)
  (bind ?max 0)
  
  (do-for-all-facts
    ((?drums db-practice_drums))
    (and (eq ?drums:type ?type) (eq ?drums:size ?size) (<= ?drums:price ?money) (>= ?drums:price ?max))
    (bind ?max ?drums:price)
    (bind ?found-model-id ?drums:id)
  )
  
  ?found-model-id
)

; ���� � ���� ������ ������ ������������� ��������� (��� ��� ���������), ��������� ������� ���������� �� ���� ������� �������
; ?money - ������ ������������
; ?type - ��� ����������� (pad, kit)
; ?size - ������ ���� (small, normal), ��� kit - ������ NA
; ���������� id ��������� ������, ��� INTEGER, ��� ������ NOTHING-FOUND, ���� ������ �� �������
(deffunction search-pr-drums-model-better (?money ?type ?size)
  (bind ?found-model-id NOTHING-FOUND)
  (bind ?min 999999)
  
  (do-for-all-facts
    ((?drums db-practice_drums))
    (and (eq ?drums:type ?type) (eq ?drums:size ?size) (> ?drums:price ?money) (< ?drums:price ?min))
    (bind ?min ?drums:price)
    (bind ?found-model-id ?drums:id)
  )
  
  ?found-model-id
)


